//
//  SelectSearchCell.swift
//  DemoappForStarIt
//
//  Created by Md. Kamrul Hasan on 23/2/19.
//  Copyright © 2019 rafiulhasan. All rights reserved.
//

import UIKit

class SelectSearchCell: UITableViewCell {

    @IBOutlet weak var SelectedUserCountryName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
