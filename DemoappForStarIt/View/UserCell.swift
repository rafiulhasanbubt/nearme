//
//  UserCell.swift
//  DemoappForStarIt
//
//  Created by sadidur rahman on 17/2/19.
//  Copyright © 2019 rafiulhasan. All rights reserved.
//

import UIKit

class UserCell: UITableViewCell {
    
    @IBOutlet weak var userNameResultLabel: UILabel!    
    @IBOutlet weak var currentCountryResultLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
