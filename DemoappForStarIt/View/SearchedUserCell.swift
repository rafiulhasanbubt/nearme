//
//  SearchedUserCell.swift
//  DemoappForStarIt
//
//  Created by sadidur rahman on 18/2/19.
//  Copyright © 2019 rafiulhasan. All rights reserved.
//

import UIKit

class SearchedUserCell: UITableViewCell {
    
    @IBOutlet weak var searchedUserResultLabel: UILabel!    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
