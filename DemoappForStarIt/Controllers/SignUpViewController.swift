//
//  SignUpViewController.swift
//  DemoappForStarIt
//
//  Created by Md. Kamrul Hasan on 15/2/19.
//  Copyright © 2019 rafiulhasan. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

class SignUpViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var firstNameTextfield: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var currentCountryTextfield: UITextField!
    @IBOutlet weak var countryOfResidenceTextField: UITextField!
    @IBOutlet weak var jobTitleTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var dismissButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //continueButton.addTarget(self, action: #selector(HandleSign), for: .touchUpInside)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }    
    @IBAction func handleDismissButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func continueButtonPressed(_ sender: Any) {
        guard let firstName = firstNameTextfield.text else { return }
        guard let lastName = lastNameTextField.text else { return  }
        guard let email = emailTextField.text else { return }
        guard let phone = phoneTextField.text else { return }
        guard let currentCountry = currentCountryTextfield.text else { return }
        guard let countryOfResidence  = countryOfResidenceTextField.text else { return  }
        guard let jobTitle = jobTitleTextField.text else { return }
        guard let password = passwordTextField.text else { return }
        
        Auth.auth().createUser(withEmail: email, password: password) { user, error in
            if error == nil && user != nil {
                print("user created")
                }
            else {
                print("Error Occured : \(String(describing: error?.localizedDescription))")
            }
        }
        let user : [String: AnyObject] = ["firstName": firstName as AnyObject,
                                          "lastName": lastName as AnyObject,
                                          "email": email as AnyObject,
                                          "phone": phone as AnyObject,
                                          "currentCountry": currentCountry as AnyObject,
                                          "countryOfResidence": countryOfResidence as AnyObject,
                                          "jobTitle": jobTitle as AnyObject,
                                          "password": password as AnyObject]
       
        UserDefaults.standard.set(countryOfResidence, forKey: "countryOfResidence")
        
        let database = Database.database().reference()
        database.child("users").childByAutoId().setValue(user)
        
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
}
