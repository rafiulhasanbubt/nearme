//
//  MenuViewController.swift
//  DemoappForStarIt
//
//  Created by Md. Kamrul Hasan on 15/2/19.
//  Copyright © 2019 rafiulhasan. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {

    @IBOutlet weak var loginButton: UIButton!    
    @IBOutlet weak var signupButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }

}
