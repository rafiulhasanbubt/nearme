//
//  SearchViewController.swift
//  DemoappForStarIt
//
//  Created by sadidur rahman on 18/2/19.
//  Copyright © 2019 rafiulhasan. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class SearchViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!{
        didSet {
            tableView.estimatedRowHeight = 120
            tableView.rowHeight = UITableView.automaticDimension
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
    var ref: DatabaseReference!
    var databaseHandle: DatabaseHandle?
    var searchedUserData: [UserModel] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        ref = Database.database().reference()
        
        databaseHandle = ref.child("users").observe(.childAdded) { (snapshot) in
            guard let actualUser = snapshot.value as? [String: AnyObject] else {
                return
            }
            
            let countryOfResidence = UserDefaults.standard.string(forKey: "countryOfResidence") ?? ""
            
            if actualUser["countryOfResidence"] as? String == countryOfResidence {
                guard let firstName = actualUser["firstName"] else { return }
                guard let lastName = actualUser["lastName"] else { return }
                guard let email = actualUser["email"] else { return }
                guard let currentCountry = actualUser["currentCountry"] else { return }
                guard let countryOfResidence = actualUser["countryOfResidence"] else { return }
                
                self.searchedUserData.insert(UserModel(
                    firstName: firstName as? String,
                    lastName: lastName as? String,
                    email: email as? String,
                    currentCountry: currentCountry as? String,
                    countryOfResidence: countryOfResidence as? String), at: 0)
                self.tableView.reloadData()
                
            }
        }
    }

}



extension SearchViewController:  UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchedUserData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchedUserCell", for: indexPath) as! SearchedUserCell
        
        cell.searchedUserResultLabel.text = searchedUserData[indexPath.row].firstName! + " " + searchedUserData[indexPath.row].lastName!
        cell.backgroundColor = UIColor(red: 0.99, green: 0.99, blue: 0.99, alpha: 0.25)
        return cell
    }
}

