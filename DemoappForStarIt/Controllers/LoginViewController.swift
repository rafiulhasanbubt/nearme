//
//  LoginViewController.swift
//  DemoappForStarIt
//
//  Created by Md. Kamrul Hasan on 15/2/19.
//  Copyright © 2019 rafiulhasan. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class LoginViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var dismissButton: UIButton!
    
    @IBOutlet weak var continueButton: RoundedWhiteButton!
    var activityView:UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        continueButton.addTarget(self, action: #selector(handleLogin), for: .touchUpInside)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @objc func handleLogin(_ sender: UIButton){
        guard let email = emailField.text else { return }
        UserDefaults.standard.set(email, forKey: "currentemail")
        
        guard let password = passwordField.text else { return }
        UserDefaults.standard.set(password, forKey: "currentpassword")
        
        Auth.auth().signIn(withEmail: emailField.text!, password: passwordField.text!) { (user, error) in
            if error == nil{
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ShowUserVC") as? ShowUserVC
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            else {
                 print("Error Occured : \(String(describing: error?.localizedDescription))")
            }            
        }
    }
    
    @IBAction func handleDismissButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
}
