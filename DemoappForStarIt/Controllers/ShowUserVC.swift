//
//  ShowUserVC.swift
//  DemoappForStarIt
//
//  Created by sadidur rahman on 15/2/19.
//  Copyright © 2019 rafiulhasan. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class ShowUserVC: UIViewController{

    @IBOutlet weak var tableView: UITableView!{
        didSet {
            tableView.estimatedRowHeight = 120
            tableView.rowHeight = UITableView.automaticDimension
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
    
    var ref: DatabaseReference!
    var databaseHandle: DatabaseHandle?
    var userData: [UserModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        self.navigationItem.backBarButtonItem = nil
        self.navigationItem.hidesBackButton = true

        ref = Database.database().reference()
        
        databaseHandle = ref.child("users").observe(.childAdded) { (snapshot) in
            guard let actualUser = snapshot.value as? [String: AnyObject] else { return }            
            let email = UserDefaults.standard.string(forKey: "currentEmail") ?? ""
            
            if actualUser["email"] as? String != email {
                guard let firstName = actualUser["firstName"] else { return }
                guard let lastName = actualUser["lastName"] else { return }
                guard let email = actualUser["email"] else { return }
                guard let currentCountry = actualUser["currentCountry"] else { return }
                guard let countryOfResidence = actualUser["countryOfResidence"] else { return }
                
                self.userData.insert(UserModel(
                    firstName: firstName as? String,
                    lastName: lastName as? String,
                    email: email as? String,
                    currentCountry: currentCountry as? String,
                    countryOfResidence: countryOfResidence as? String), at: 0)
                self.tableView.reloadData()
            }
        }
    }
        
    @IBAction func searchButtonTapped(_ sender: Any) {
       // let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        //present(nextViewController, animated: true, completion: nil)
        navigationController?.pushViewController(nextViewController, animated: true)        
    }
    
    @IBAction func logOutAction(_ sender: UIBarButtonItem) {
        try? Auth.auth().signOut()
        self.dismiss(animated: true, completion: nil)
    }
}


extension ShowUserVC:  UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell", for: indexPath) as! UserCell
        
        cell.userNameResultLabel.text = userData[indexPath.row].firstName! + " " + userData[indexPath.row].lastName!
        cell.currentCountryResultLabel.text = userData[indexPath.row].currentCountry!
        cell.backgroundColor = UIColor(red: 0.99, green: 0.99, blue: 0.99, alpha: 0.25)
        return cell
    }
    
    
}
