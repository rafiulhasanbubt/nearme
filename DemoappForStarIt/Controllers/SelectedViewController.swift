//
//  SelectedViewController.swift
//  DemoappForStarIt
//
//  Created by Md. Kamrul Hasan on 23/2/19.
//  Copyright © 2019 rafiulhasan. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class SelectedViewController: UIViewController {
    @IBOutlet weak var selectedCountryNameView: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var selectedCountry: String?
    
    var ref: DatabaseReference!
    var databaseHandle: DatabaseHandle?
    var searchedUserData: [UserModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        selectedCountryNameView.text = selectedCountry
        
        tableView.delegate = self
        tableView.dataSource = self
        
        ref = Database.database().reference()
        
        databaseHandle = ref.child("users").observe(.childAdded) { (snapshot) in
            guard let actualUser = snapshot.value as? [String: AnyObject] else {
                return
            }
            
            let countryOfResidence = UserDefaults.standard.string(forKey: "countryOfResidence") ?? ""
            
            if actualUser["countryOfResidence"] as? String == countryOfResidence {
                guard let firstName = actualUser["firstName"] else { return }
                guard let lastName = actualUser["lastName"] else { return }
                guard let email = actualUser["email"] else { return }
                guard let currentCountry = actualUser["currentCountry"] else { return }
                guard let countryOfResidence = actualUser["countryOfResidence"] else { return }
                
                self.searchedUserData.insert(UserModel(
                    firstName: firstName as? String,
                    lastName: lastName as? String,
                    email: email as? String,
                    currentCountry: currentCountry as? String,
                    countryOfResidence: countryOfResidence as? String), at: 0)
                self.tableView.reloadData()
                
            }
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
}


extension SelectedViewController:  UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchedUserData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "selectedCell", for: indexPath) as! SelectSearchCell
        
        cell.SelectedUserCountryName.text = searchedUserData[indexPath.row].firstName! + " " + searchedUserData[indexPath.row].lastName!
        cell.backgroundColor = UIColor(red: 0.99, green: 0.99, blue: 0.99, alpha: 0.25)
        return cell
    }
}
