//
//  UserModel.swift
//  DemoappForStarIt
//
//  Created by sadidur rahman on 17/2/19.
//  Copyright © 2019 rafiulhasan. All rights reserved.
//

import Foundation
import UIKit

struct UserModel {
    let firstName: String!
    let lastName: String!
    let email: String!
    //let phone: String!
    let currentCountry: String!
    let countryOfResidence: String!
//    let jobTitle: String!
//    let password: String!
    
}
